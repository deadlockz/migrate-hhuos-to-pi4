# NOW := $(shell date +"%c" | tr ' :' '__')
#NOW := $(shell date +"%Y%m%d-%H%M")
OUTFILE = Masterarbeit_Peters.pdf

MDFILES = $(wildcard *.md)

all: target pdf

target:
	for MDFILE in $(MDFILES) ; do \
		if [[ "$$MDFILE" == "readme.md" ]] ; then \
			continue ; \
		fi ; \
		sed -e 's: ,,: \\glqq{}:g' $$MDFILE > temp_$$MDFILE ; \
		sed -i "s#'' #\\\\grqq{}#g" temp_$$MDFILE ; \
		sed -i -e 's:\.png):\.pdf):g' temp_$$MDFILE ; \
		sed -i -e 's:~~~java:\\begin{minted}{java}:g' temp_$$MDFILE ; \
		sed -i -e 's:~~~cpp:\\begin{minted}{cpp}:g' temp_$$MDFILE ; \
		sed -i -e 's:~~~bash:\\begin{minted}{bash}:g' temp_$$MDFILE ; \
		sed -i -e 's:~~~gas:\\begin{minted}{gas}:g' temp_$$MDFILE ; \
		sed -i -e 's:~~~:\\end{minted}:g' temp_$$MDFILE ; \
		pandoc --top-level-division=chapter --highlight-style=pygments -f markdown+multiline_tables temp_$$MDFILE -o temp_$$MDFILE.tex ; \
		sed -i -r -e 's|\\caption\{fig:(.*):|\\caption\{\\label\{fig:\1\}|g' temp_$$MDFILE.tex ; \
		sed -e "s#graphics{fig/#graphics[width=160mm]{fig/#g" temp_$$MDFILE.tex > $$MDFILE.tex ; \
	done
	rm -rf temp*

pdf:
	pdflatex -shell-escape readme.tex
	#latex -shell-escape readme.tex
	bibtex readme
	pdflatex -shell-escape readme.tex
	pdflatex -shell-escape readme.tex
	#latex -shell-escape readme.tex
	#latex -shell-escape readme.tex
	#dvips -Ppdf readme.dvi
	#ps2pdf readme.ps
	mv readme.pdf $(OUTFILE)

clean:
	rm -rf _minted-readme
	rm -rf temp*
	rm -f *.lot *.dvi *.idx *.lof *.log *.ps *.out *.xml *.toc *.bib *.aux *.bbl *.blg

