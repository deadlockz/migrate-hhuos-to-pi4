# Porting the operating system hhuOS from x86 to the 64bit System-on-a-Chip of the Raspberry Pi4 (Cortex-A72)

It is an overview of different Raspberry Pi versions, different operating systems used by educators and the description of the Raspberry Pi4 architecture. In addition, a number of useful references to the ARM architecture and its peculiarities are mentioned. Alternatives to the Raspberry Pi are also presented, on which comparable minimal operating systems have been developed. Furthermore, the concrete implementation of a minimalistic operating system in C++ for the Pi4 is presented.

# Portierung des Betriebssystems hhuOS von x86 auf das 64bit Ein-Chip-System des Raspberry Pi4 (Cortex-A72)

Dies ist eine (schlechte) Alternative zur [PDF](Masterarbeit_Peters.pdf),
da nicht alle Referenzierungen und Fußnoten klappen werden.

[Abstract](ABSTRACT.md)

**Inhaltsverzeichnis**

- [Einleitung](INTRO.md)
  - [Motivation](MOTIVE.md)
  - [Aufbau der Arbeit](OVERVIEW.md)
- [Grundlagen](BASICS.md)
- [Verwandte Arbeiten](COMPARE_TO.md)
- [Architektur des Raspberry Pi4](MAIN.md)
- [Implementierung](IMPLEMENT.md)
- [Fazit und Ausblick](FAZIT.md)

