
In diesem Kapitel werden kurz die Betriebssysteme Minix, Topsy, xv6 und
OOStuBS vorgestellt und vergleichbare Portierungen auf den Raspberry Pi.
Ebenso wird ein zum Raspberry Pi alternativer Kleinst-Rechnern (MaxiMite)
und dessen Betriebssystem und Hardware-Konzept vorgestellt. Mit
freeRTOS (free RealTime-OS) und der Circle-Bibliothek werden zwei
Frameworks vorgestellt, mit denen eine Betriebssystem-Entwicklung
auf dem Raspberry Pi möglich ist. Weiterhin werden Tutorials bzgl.
der Baremetal-Entwicklung[^whatbaremetal]
auf dem Pi und der Migration von Linux auf den Pi vorgestellt.

Die Auswahl der vorgestellten Systeme ergibt sich aus folgenden Schwerpunkten:

- Dokumentationsumfang des Systems
- Einsatz des Systems in der Lehre
- Affinität zu Kleinst-Rechnern und dem Raspberry Pi4
- Primär in C oder C++ geschrieben und nicht in Rust \cite{rusttutonpi} oder 
  BBC Basic[^bbcriscos]

Es wird gezeigt, dass die vorgestellten Systeme alle kleiner Mängel aufweisen.
Entweder ist ihr Code-Umfang sehr groß und hält Studierende davon ab,
alle Software Design-Entschei\"-dungen dieser Systeme im Detail
zu verstehen (OOStuBS, FreeRTOS, Circle, Minix), oder
das System ist klein, aber auf eine sehr spezielle Hardware
zugeschnitten (xv6 auf RISC-V, MMBasic auf PIC32, Rpi OS auf Pi3).
In C++ sind nur Circle und OOStuBS geschrieben, die mit weit
über 10000 Codezeilen als Grundsystem extrem umfangreich sind.
Topsy stellt durch seine Portierung auf ia32 (x86 seit Pentium 4)
eine interessante Entwicklung dar, auch wenn diese Zielplattform
inzwischen 20 Jahre alt ist. Das System xv6 hat hingegen mit seiner Portierung
auf RISC-V einen Schritt zu einer modernen Hardware gemacht.
RISC-V Entwicklungs-Boards sind im Vergleich zum Pi1 oder Pi2
mit ähnlichem Leistungsumfang jedoch teurer.

Mit dem Pi4 als moderne Hardware, gibt es bisher nur
Linux und Circle, die den neuen Interrupt-Controller GIC-400
verwenden. Es existieren keine C oder C++ Baremetal-Beispiele
weder im 32-Bit noch im 64-Bit Ausführungs-Modus des Pi4, die
diesen neuen Interrupt-Controller verwenden[^examplegic400]. Ebenso existieren
keine Baremetal-Beispiele, die einen Task-Wechsel auf der
Basis eines Interrupts beim Pi4 ermöglichen. Mit der Implementierung
von hhuOS-for-Pi4 sollen diese
Lücken an Baremetal-Beispielen für den Pi4 geschlossen werden
und eine Basis für ein kleines Betriebssystem für die Lehre auf
einer moderner 64-Bit Multicore Hardware geschaffen werden.


## Vergleichbare Arbeiten zur OS-Entwicklung

### Minix

Minix wurde 1987 als Beispiel für einen Microkernel-Betriebssystem entwickelt.
Es wurde von Prof. Andrew S. Tanenbaum in C geschrieben und
in Form von acht 360kB-Disketten verteilt. Auf diesen Disketten war
das System und der Quellcode, der aus etwas
über 20000 [Codezeilen](#codeminix) besteht \cite{minixdiscs}.
Es lief auf dem Intel 8088 Chip, der ein günstiger 0'86er war mit einem
8-Bit statt einem 16-Bit externen Bus-Zugriff.
So konnte man eine 16-Bit CPU nutzen, die jedoch in eine 8-Bit
Peripherie verbaut werden konnte.
Da der Minix-Quellcode im Benutzerhandbuch veröffentlicht wurde und der
Verlag des Buchs damals nicht bereit war, diesen Teil des Buchs als frei
kopierbar zu erlauben, konnte der Quellcode streng genommen nur
durch den Kauf des Buchs legal erworben werden. Der Quellcode neuerer Minix
Versionen hat inzwischen eine BSD-3-Lizenz.
Bei Minix wird der Schwerpunkt in einer erhöhten Sicherheit und Zuverlässigkeit
des Betriebssystems auf Microkernel-Basis gelegt, was in den
Veranstaltungen von Prof. Tanenbaum besonders hervorgehoben
wird \cite{minixreli}.

Eine Portierung von Minix auf ARM ist bei einem SoC, ähnlich
dem Raspberry Pi, gelungen \cite{minixonarm}. Es handelt sich um
BeagleBone-Boards mit einem Singlecore 32Bit Cortex-A8
Prozessor[^oldcortexa8] für ca. 45,- EUR[^notcheapbeagle]
und Video- und Sound-Ausgabe wird von Minix auf diesen
Boards noch nicht unterstützt.

| Bedeutung       | Codezeilen |
|:----------------|-----------:|
| Dateizugriff    |  3590      |
| Drucker         |   169      |
| Shell-Befehle   | 11763      |
| Heap Verwaltung |  1317      |
| Scheduler       |   242      |
| Kernel (Rest)   |  3166      |
| **Summe:**      | **20247**  |

: Codezeilen von Minix 1.1 nach Bedeutung aufgefächert[.]{#codeminix}

### xv6

Im Herbst 2002 hat das Massachusetts Institute of Technology in Cambridge
einen Kurs zur Betriebssystem-Entwicklung angeboten. Dieser Kurs basierte
auf dem Unix V6 Quellcode und einer umfangreichen Code-Dokumentation
sowie einem Buch von John Lions über V6. Laut der Aussage der Projekt-Webseite
hatte sich gezeigt, dass die Studierenden Probleme mit einem seit 30 Jahre
veralteten Betriebssystem haben, was in einer Ur-Version von
C[^KandRc] geschrieben ist und für eine ebenso veraltete Architektur (PDP-11)
ausgelegt ist \cite{xv6link}.
Im Sommer 2006 hatte man deswegen V6 soweit umgeschrieben, dass
im Herbst 2006 der Kurs nach fast 4 Jahren Pause wieder angeboten wurde.
Das entstandene System nannte man *xv6*, ist in
ANSI C geschrieben und auf 3'86er portiert worden \cite{xv6x386}. Das
gesamte System besteht aus knapp 6000 [Codezeilen](#codexv6).
Anstelle des Aus- und Einschaltens von Interrupts zur Prozess-
Synchronisation seien ,,Locks und Threads''  eingebaut worden \cite{xv6spinlock}.
Es ist außerdem
Multiprozessor-fähig und nutzt dafür den APIC[^whatapic].
Parallel wurde eine Version für die 
offene RISC-V Architektur entwickelt \cite{xv6riscv}. Ab 2013 existiert
eine Portierung von xv6 auf die ARMv6 Architektur des
Raspberry Pi1 \cite{xv6pi}. Inzwischen gibt es auf Github.com Entwicklungen
für den Raspberry Pi2 und Pi3 \cite{xv6pi2}. Seit August 2020 wird die
3'86er Version nicht mehr weiter gepflegt und nur noch die RISC-V Version
weiterentwickelt. Hier sei auch bemerkt: trotz der Weiterentwicklung kann
bei keinem der xv6 Systeme bis heute eine USB-Tastatur angeschlossen
werden; in den Code-Veröffentlichung sind kein entsprechenden Treiber zu finden.

| Bedeutung       | Codezeilen |
|:----------------|-----------:|
| APIC            |   315      |
| Dateizugriff    |  1108      |
| Shell-Befehle   |   471      |
| Heap Verwaltung |   223      |
| Scheduler       |   610      |
| Kernel (Rest)   |  3267      |
| **Summe:**      | **5994**   |

: Codezeilen der ersten xv6 Version nach Bedeutung aufgefächert[.]{#codexv6}

### OOStuBS

Das ObjektOrientierte Studenten BetriebsSystem -- kurz OOStuBS, oder nur StuBS -- wird
an mehreren Universitäten in der Lehre eingesetzt. Im Internet finden sich
Kurse und Unterlagen an den Universitäten Erlangen-Nürnberg, Hannover,
TU-Dortmund[^tudort], TU-Hamburg und der Uni-Osnabrück \cite{faustubs}, \cite{oostubs_doxy}, \cite{tuhhgit}, \cite{uniosna}.
Der C++ Code ist mit ca. 12700 [Codezeilen](#codeoostubs) sehr umfangreich,
obwohl es sich hierbei nur um ein Grundsystem handelt, welches durch
Übungen vervollständigt wird \cite{tuhhgit}. Als
Zielplattform ist der 3'86er im 32-Bit Modus und die Verwendung des
APIC als Interrupt-Controller vorgesehen. 
Die Uni-Erlangen-Nürnberg (FAU)[^whatfau] bot Studierenden den
Zugriff auf vier Testrechnern an und in der Einführung wurde der
Umgang mit QEMU und KVM gezeigt. Folgende Bereiche müssen im Rahmen von Übungsaufgaben
dem StuBS beigefügt werden:

- Stringverarbeitung und Text Ein- & Ausgabe (CGA, PS/2 Tastatur)
- Eine Anwendung (je Kern) und Umgang Nebenläufigkeit (Interrupt, Spinlock)
- Prozesswarteschlange, Scheduling, Prolog/Epilog von Threads
- (L)APIC Timer, Interprozessor-Interrupts
- Idle (energiesparend), warten, PC-Speaker

Die Übungsaufgaben und der Code weisen
Ähnlichkeit mit dem Grundsystem aus dem Wintersemester an der hhu
von 2016 auf. Unterschiede
gibt es vor allem bei der Nutzung
des PIC[^whatpic]
anstelle des deutlich komplexeren APIC, sowie im Konzept des präemptiven Multitaskings.
Mit MPStuBS gibt es auch eine
Multicore-Version von StuBS, deren Besonderheiten
in den Übungsaufgaben behandelt werden, sollten sich die Studierenden
für die Multicore-Version als Grundsystem entscheiden. In der
Ruhmeshalle,
die die besten Arbeiten zeigt, wird auch eine 64-Bit Version (2018) von
StuBS aufgeführt \cite{ruhmhalle}.

| Bedeutung          | Codezeilen |
|:-------------------|-----------:|
| Anwendungen        |    1317    |
| Dateizugriff       |    6618    |
| Heap Verwaltung    |     238    |
| *Interrupt (leer)* |     107    |
| *Sync (leer)*      |     233    |
| *Scheduler (leer)* |      64    |
| Kernel (Rest)      |    3797    |
| **Summe:**         | **12691**  |

: Codezeilen des Grundsystems von OOStuBS nach Bedeutung aufgefächert[.]{#codeoostubs}

### MMBasic und Kleinst-Rechner

Der fließende Übergang zwischen einem Rechner und einem Mikrocontroller
wird beispielsweise am MaxiMite-Projekt deutlich. Bei diesem Projekt
(und der DuinoMite-Serie von oLimex) ging es um eine einfache
Möglichkeit, für einen 32-Bit Mikrocontroller schnell Programme zu schreiben
und zu debuggen \cite{maximite}, \cite{duinomite}. Dazu hat man auf einem Mikrocontroller einen
BASIC Interpreter als Betriebssystem eingespielt (**MMBASIC**). Die 
übliche Anwendung
eines Mikrocontrollers ist die Ausführung eines speziellen Programms,
welches in diesem Fall der BASIC Interpreter ist. Zusätzlich, damit man
es als Betriebssystem bezeichnen kann, verfügt
der Interpreter über eine Eingabe-Shell, die seriell oder durch den
Anschluss einer PS/2 Tastatur bedient werden kann. Zur Ausgabe ist
der Anschluss eines VGA-Monitors (nur Weiß) oder via Composite möglich.
Über einen SPI-Bus ist der
Anschluss einer SD-Karte - quasi als Festplatte - vorgesehen. Dort
sind BASIC-Programme als Text-Dateien lad- und speicherbar. Mit 7234
[Codezeilen](#codemmbasic) ist dieses Single-Thread Betriebssystem sehr
klein. Als Mindestanforderung brauchen Mikrocontroller mindestens 94kB
Flash Speicher für das System sowie 16kB
Arbeitsspeicher zur Ausführung von Programmen \cite{mmbasicRequirements}. 

| Bedeutung        | Codezeilen |
|:-----------------|-----------:|
| Editor Anwendung |   1133     |
| Dateizugriff     |   1360     |
| I2C Bus          |    817     |
| CAN Bus          |    566     |
| Keyboard         |    767     |
| Kernel (Rest)    |   2177     |
| **Summe:**       | **7234**   |

: C-Codezeilen von MMBasic nach Bedeutung aufgefächert[.]{#codemmbasic}

Als Lernsystem ist dieses Projekt in vielerlei Hinsicht interessant:
Hardware und Software sind Open-Source,
die Hardware ist außerdem günstig und übersichtlich verdrahtet.
Den Kleinst-Rechner könnte man auch selber bauen. Wenn man auf Tastatur
und VGA Anschluss verzichtet und eine serielle Console mit TFT-Display
reicht, ist das MicroMite-Projekt interessant, welches ein gutes
Tutorial zu Bauteilen und Funktionen hat \cite{micromite1}, \cite{micromite2}.
Für Schüler`*`innen gilt BASIC als leicht erlernbar und da Entwicklungs-Software und
Programmier-Geräte bei diesem Kleinst-Rechner wegfallen, erleichtert
es den Einstieg in die Hardware-nahe Programmierung.

Preislich ist die DuinoMite-Serie von oLimex vergleichbar mit
dem Raspberry Pi1. Je nach Lernziel und Alter der Schüler`*`innen
haben DuinoMite, MicroMite und Pi1 ihre individuellen Vorteile.


### Topsy

Topsy (=Teachable Operating System) ist 1996 an der ETH Zürich entwickelt
worden. Es ist in C und Assembler geschrieben und für ein MIPS-RISC
Entwicklungsboard[^mipsriscboard] oder einem entsprechendem Emulator
ausgelegt. Später wurden auch Portierungen auf 3'86er gemacht sowie
ab Topsy v3 (mit Microkernel) auf ia32 \cite{topsypentium}. In den ca.
7100 [Codezeilen](#codetopsy) sind im Gegensatz zu MMBasic und den
MaxiMite-Boards Funktionen wie virtueller Adressraum und ein Scheduling
implementiert. In der Anleitung zu Topsy 1.1 wird grob
skizziert, warum man zur Lehre ein eigenes System entwickelt hat \cite[S.~5]{topsysource}.
Die Gründe wurden auf folgende Punkte zusammengefasst:

- populäre Betriebssysteme sind funktionsüberfüllt
- aus Effizienz-Gründen werden oft unverständliche Hacks statt
  übersichtlich strukturierter Code verwendet
- aufgrund von Hardware-Kompatibilitäts-Gründen zu älterer Hardware
  sind schwer nachvollziehbare Codeanpassungen vorhanden 

Der Code von Topsy ist daher entsprechend simple aufgebaut und enthält
nur das nötigste an Peripherie. Zur Ein- und Ausgabe diente die serielle
Schnittstelle. Neben Tastatur und Videoausgabe wurde auch auf
ein Dateisystemzugriff in Topsy verzichtet (siehe \cite[S.~2]{topsypfattner}).
Beim Quellcode wird großer Wert auf die Trennung zu
Hardware-abhängigen Implementierungen gelegt. Die Trennung zwischen
User- und Kernel-Space ist in je fünf bis sieben Systemcalls für
die Module Memory-, Thread- und
IO-Management aufgeteilt[^topsycalls].
Zur Koordinierung zwischen den Modulen und den Anwendungen
im User-Space ist das *Topsy*-Modul verantwortlich.

| Bedeutung            | Codezeilen |
|:---------------------|-----------:|
| Anwendungen          |   1548     |
| Treiber Seriell/FPGA |   2084     |
| Heap Verwaltung      |    754     |
| Scheduler            |   1505     |
| Kernel (Rest)        |   1258     |
| **Summe:**           | **7149**   |

: Codezeilen von Topsy v1.1 (MIPS) nach Bedeutung aufgefächert[.]{#codetopsy}

### freeRTOS

Das Open-Source Real-Time Operating
System -- kurz: freeRTOS -- füllt den
Bereich zwischen der hardwarenahen Singlecore-Einzelanwendungs-Entwicklung 
auf Mikrocontrollern und der Anwendungs-Entwicklung mit einer
Hochsprache \cite{whatfreertos}. Es handelt sich bei \nohyphens{freeRTOS} um ein modular aufgebautes Framework
in C, und ist für verschiedene Architekten verfügbar. Mikrocontroller
werden in der Industrie oft in zeitkritischen Anwendungen genutzt, wo
eine Garantie für eine Reaktionszeit auf ein Ereignis gegeben sein muss.
Das präemptives Multitasking in freeRTOS erfüllt diese Bedingung.
Um das Betriebssystem klein zu halten, handelt es sich bei freeRTOS
nur um ein Framework, mit dem man seine Anwendungen für die Zielplattform
entwickeln kann. Chip-Hersteller bieten freeRTOS (oder Derivate)
zusammen mit nicht-freien oder lizenzpflichtigen Bibliotheken an, die
in einer umfangreichen Entwicklungsumgebung eingebunden sind.
Beispiele dafür sind der *MPLAB Harmony Configurator*
vom Chip-Hersteller Microchip \cite{harmony} sowie die Einbindung
von RTOS[^renesas2] in e2-Studio[^renesas1],
der Entwicklungsumgebung des Chip-Herstellers Renesas.
Offiziell wird 64-Bit (seit ARMv8) in freeRTOS nur in
der ARM Mikrocontroller-Serie (Endung *-M*) und in einem Simulator
unterstützt \cite{freertosports}, aber auf Github
existiert auch eine 64-Bit Portierung für den Pi3, der
mit dem Cortex-A53 zur ARM Application-Serie (Endung *-A*) gehört \cite{freertos64bitarm}.
Diese Portierung von freeRTOS unterstützt Pi1, Pi2 und Pi3. Ein
mit ca. 500 Codezeilen sehr umfangreicher Assembler Boot-Code
erkennt die Raspberry Pi Version und ggf. Unterschiede in der
Hardware. Die restlichen 33 Dateien bestehen aus
ca. **10372 Codezeilen**. Folgende Funktionen unterstützt
dieses freeRTOS Grundsystem:

- präemptives und kooperatives Multitasking
- Semaphoren und Timer
- Multicore
- Ein- und Ausgabe: nur seriell
- Speichermanagement

Es sind fünf Arten implementiert, Speicher zu allokieren. Darunter ist auch
eine *Gready*-Version wie im Grundsystem von hhuOS, die kein *free*
durchführt. Außerdem hat der Entwickler in einzelnen Projekten
dieses Grundsystem um folgende Funktionen erweitert:

- Text/Grafik Ausgabe
- GPU Nutzung für GLES und OpenGL
- USB-Tastatur-Erkennung
- Dateizugriff (FAT32) auf die SD-Karte

Speziell für GLES und OpenGL war es von Vorteil, dass es für die GPU
(VideoCore IV), die in den Raspberry Pis 1 bis 3 verbaut ist,
bereits eine Open-Source Implementierung \cite{vc4fw} gibt sowie
entsprechende Dokumentationen \cite{vc4wiki1} \cite{vc4wiki2}. Auch
ist die USB-Anbindung bei Pi1 bis Pi3 leichter, da hier der Umweg
über einen PCIe-Bus (beim Pi4) entfällt.

## Pi Baremetal-Code-Tutorials

### Circle Bibliothek

Circle ist eine Open-Source Bibliothek, die primär in der Sprache C++
geschrieben ist und für Baremetal-Anwendungen aller vier Pi
Versionen gemacht ist \cite{circle}. Sowohl 32-Bit als auch 64-Bit
Entwicklung, falls es der Pi unterstützt, sind möglich.
Ein kooperatives Scheduling ist bereits
implementiert sowie die Nutzung aller verfügbaren Kerne. Fast alle
Hardware-Funktionen des Pi können mit der Circle-Bibliothek genutzt
werden. Neben TCP/IP Funktionen (vorrangig via USB-Ethernet Adapter)
ist inzwischen experimentell auch die Nutzung von WLAN möglich. Vom
Pi4 wird auch der Gigabit-Adapter unterstützt. Bluetooth und die
Pi Camera Schnittstelle werden noch nicht
unterstützt[^circlenotall].

Bei hhuOS-for-Pi4 wurde zur Implementierung
regelmäßig Bezug auf den Code der Circle-Bibliothek genommen. Sie
konnte allerdings nicht komplett zur Implementierung verwendet werden,
da aufgrund ihres Funktionsumfangs und der Hardware-Kompatibilität
zu anderen Pi Versionen extrem viele Präprozessor-Weichen und
Verzweigungen im Code aufweist. Die Lesbarkeit dieses Quellcodes ist
so extrem erschwert. Hinzu kommt viel zusätzlicher Code zum Debuggen,
wenn die Circle-Bibliothek mit unerwarteten Eingaben konfrontiert
wird. Sie ist darauf ausgelegt, dass man mit ihr eine Anwendung
gemäß einer API schreibt, ohne in den Quellcode der Bibliothek eingreifen zu
müssen.

Beschränkt man sich bei der Circle-Bibliothek nur auf
die Ordner `lib/` und `include/`, findet man ca. 400
Dateien und über **48000** Codezeilen. Für
das hhuOS-for-Pi4 hätte man viel kürzen und anpassen
müssen, um den Code in einer Veranstaltungsreihe in Gänze verstehen
zu können. In \cite{pinto2013operating} werden die Herausforderungen
beschrieben, ein Betriebssystem vom Quellcode her so aufzubauen, dass es
den besonderen Herausforderungen bei der Implementierung, dem Verständnis
von Hardware und Architektur sowie didaktischen Ansprüchen genügt.
Genauso wie bei [Topsy] kam man zu dem Ergebnis,
dass nur eine Neuprogrammierung eines OS
diesen Ansprüchen gerecht werden kann[^thiswastempos].

Diese Bibliothek ist kein und hat kein Tutorial. Sie
stellt aber ein Beispiel für eine C++ Baremetal-Entwicklung auf
dem Raspberry Pi dar und wird daher an dieser Stelle erwähnt.

### Linux auf dem Pi

Die Entwickler`*`innen des Raspberry Pi haben sich für eine eigene
Linux Kernel Entwicklung entschieden, auch wenn es Teile ihres
Kernels zurück in den Upstream Kernel
schaffen \cite{piupstreaming}. Diese Linux-Version ist zwar nur für Raspberry Pis zugeschnitten,
dennoch greifen weiterhin die Argumente bzgl. eines für die
Lehre unnötigen Feature-Umfangs sowie erschwerte Code-Lesbarkeit durch
Präprozessor-Weichen und Effizienz-Hacks. Wie schon in den
Abschnitten [Topsy] und [Circle Bibliothek] genannt wurde, ist auf eine solch
komplexe Code-Basis in der Lehre nur bedingt referenzierbar. 2018 wurde in dem
Projekt *RPi OS*
ein reduziertes Linux System für die Lehre begonnen \cite{linuxtopi}.
In einzelnen Lektionen (und Übungen)
wird dieses System, was *nur für den Pi3* ausgelegt ist, vorgestellt
und durch die Lektionen erweitert. In jeder Lektion wird *RPi OS* mit
Linux verglichen, um auch den Quellcode von Linux besser verstehen zu können.
Die letzte Lektion und somit der einzige nicht veröffentlichte Teil ist
das 1:1 Mapping von physikalischen Adressen auf virtuelle Adressen.
An dieser Stelle bleibt es bei dem Projekt offen, wie und ob
Tasks in Zukunft auf ein Flat-Memory Modell zugreifen können.

| Bedeutung                 | Codezeilen |
|:--------------------------|-----------:|
| Treiber Seriell           |     83     |
| printf                    |    200     |
| Heap Verwaltung (Array)   |    172     |
| Completely Fair Scheduler |    245     |
| Kernel (Rest)             |    590     |
| **Summe:**                | **1290**   |

: Codezeilen von RPi OS nach Bedeutung aufgefächert[.]{#coderpios}

Der Code (Lesson06) ist mit 1290 [Codezeilen](#coderpios) extrem übersichtlich,
auch wenn 24% davon Assembler ist. Auf eine Eingabe via Tastatur
und Grafik-Ausgabe wird verzichtet. Wie bei ähnlichen minimalistischen
Projekten ist die Ein- und Ausgabe auf eine serielle Konsole
beschränkt. In der Einleitung \cite{linuxminimal} steht, 
dass spezielle Algorithmen zur Performance,
Sicherheits-Techniken (außer virtuellen Speicher), Multicore- und Synchronisations-Techniken
oder Millionen von Funktionen eines fertigen Betriebssystems
nicht umgesetzt worden. Dies erklärt die geringe Menge an
Codezeilen des Projekts, da es sich nicht
um eine komplette Linux Portierung handelt und von Anfang an
als nur die serielle Schnittstelle genutzt wird. Viele Lektionen, die
den Linux-Code noch hätten detaillierter erklären können, sind nicht
vorhanden[^endofrpios].


### Baremetal auf dem Pi

Die umfangreichsten Raspberry Pi Tutorials, die sich mit dem
Pi ohne ein Betriebssystem auseinander setzen, sind alle in C
und nicht in C++ geschrieben. Darunter ist das Tutorial mit
vielen Beispielen von David Welch \cite{dwelch}
mit einem Fokus auf 32-Bit Code und den Pi1 bis Pi3. Nur
wenige Beispiele und noch weniger Beschreibung findet sich
dort zum Pi4. Im Tutorial \cite{pi3tut} von Zoltan Baldaszti wird der Fokus
auf die 64-Bit Micro-Architektur des Pi3 gelegt. Es
konzentriert sich auf die Schnittstellen des
Pi und dessen Verwendung und ist mehr eine Beispiel-Sammlung und
weniger ein Tutorial. Die Readme-Dateien
zu den Beispielen sind knapp verfasst und auf ein zu erwartendes
Ergebnis bei der Ausführung fixiert mit einer Art API Beschreibung.
Wesentlich detaillierter ist das Tutorial \cite{pi4tut} von Adam
Greenwood-Byrne, was sich auf den Pi4 (und auch 64-Bit) konzentriert.
Es werden hier allerdings nur die Schnittstellen vorgestellt, die für eine
Beispiel-Anwendung[^gamepi4]
nötig sind. Diese Beschreibungen sind umfangreich
und führen Schritt für Schritt zur nächsten Funktion, deren
Implementierung erklärt wird.

Da der Raspberry Pi bei allen
Tutorials als Mikrocontroller mit HDMI Ausgabe angesehen
wird und nicht als Basis für ein Betriebssystem, auf dem
mehrere Anwendungen laufen, findet man darin
auch keine Scheduler mit Tasks. Lediglich
ein Tutorial
für den 32-Bit Pi2 hat ein präemptives Multitasking als
Beispiel aufgeführt \cite{pi2multitask}. Darin enthalten ist aber auch ein
nicht atomarer Mutex via Compare-And-Set, wie man dem Quellcode
entnehmen kann[^pi2fakecas].
Primär werden Multicore-Funktion von Tutorials nur in sofern
aufgegriffen, in dem jedem Kern eine eigene Anwendung mit
Speicher zugeteilt wird. Es findet sich auch kein Beispiel, wie
man den neuen Interrupt-Controller[^thegic400] des Pi4 verwenden
kann, der für ein präemptives Multitasking via Timer-Interrupt
in hhuOS-for-Pi4 nötig war.


[^whatbaremetal]: Baremetal-Entwicklung: Entwicklung auf der Hardware ohne eine Abstraktion durch ein Betriebssystem
[^bbcriscos]: BBC Basic in RiscOS: \url{https://www.riscosopen.org/}
[^examplegic400]: Dieses Beispiel entstand im Rahmen dieser Masterarbeit: \url{https://github.com/no-go/pi4-irq-SystemTimer}
[^oldcortexa8]: Cortex-A8: Markteinführung 2005
[^notcheapbeagle]: Preis BeagleBoard: \url{https://beagleboard.org/bone}
[^whatapic]: Advanced Progammable Interrupt Controller
[^whatfau]: Friedrich-Alexander-Universität Erlangen-Nürnberg
[^whatpic]: Programmable Interrupt Controller, Abwärtskompatibilität zum Chip i8259 aus der 0'86er Architektur
[^mipsriscboard]: IDT MIPS R3052E
[^topsycalls]: \url{https://github.com/pahihu/Topsy/blob/master/Topsy/Syscall.h}
[^circlenotall]: Stand: 20.09.2021
[^thiswastempos]: in diesem Fall nannte man es TempOS
[^gamepi4]: Spiel: Blockout, via Bluetooth steuerbar
[^pi2fakecas]: Commit \href{https://github.com/sokoide/rpi-baremetal/commit/d9e857c6}{d9e857c6}: Datei `startup.s` Zeile 288 in \cite{pi2multitask}
[^thegic400]: GIC-400
[^renesas2]: Vorteile für RTOS Nutzung: \url{https://www.youtube.com/watch?v=ECEvUEkSSLg}
[^renesas1]: RTOS in e2-Studio: \url{https://www.youtube.com/watch?v=ki5ibmc0yMo}
[^tudort]: Vorlesungsverzeichnis Sommersemester 2019: \url{https://www.lsf.tu-dortmund.de}
[^KandRc]: entwickelt von Kernighan and Ritchie: auch K&R C genannt
[^endofrpios]: Ende mit Lesson06 im Juni 2018

