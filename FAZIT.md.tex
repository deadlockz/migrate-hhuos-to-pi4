Beim Bootvorgang ist vieles auf die Closed-Source Startdateien des Pi4
ausgelagert worden. Das Erzeugen eines Bootmediums, die Suche nach einer
Markierung auf einem Datenträger, der Zugriff auf einzelne Blöcke und
der umständliche real-Mode fallen in hhuOS-for-Pi4 weg. Der Zugriff auf
den Speicher via \texttt{sections} und die Begrenzung auf 20-Bit oder
24-Bit Adressen fallen bei ARM ebenfalls weg. Das erleichtert den
Zugriff auf den gesamten physikalischen Adressraum. In diesen Punkten
ist das Grundsystem also einfacher geworden.

Grundsätzlich ist die Dokumentation des Pi4 durch den Hersteller im
Nachhinein doch sehr unbefriedigend, da ein großer Teil des Bootvorgangs
Closed-Source ist und die Dokumentation des Pi4 auf ein bereits
gestartetes Linux ausgelegt ist. In den vergangenen Kapiteln ist ein
Überblick der problematischen Stellen beim Booten des Pi4 geschaffen
worden. Dies ermöglicht entsprechende Recherchen und Maßnahmen für eine
weitere Baremetal-Entwicklung auf dem Pi4 anzustoßen, falls diese den
Bootvorgang betreffen.

In den Dateien \texttt{startup.S}, \texttt{Calls.hpp} und
\texttt{Calls.cpp} ist Assembler-Code zu finden. \texttt{startup.S}
enthält die Konfiguration für den initialen Speicherzugriff (Stack) und
nötige Einstiegspunkte für Bluescreen, Calls, Threadwechsel und
Interrupts. In \texttt{Calls.hpp} ist mit \emph{define} eine
Breakpoint-Operation (\texttt{BRK}) in Assembler hinterlegt und in der
\texttt{Calls.cpp} sieben sehr gleiche Supervisor-Call-Operationen
(\texttt{SVC}). Die Supervisor-Calls ließen sich leider nicht mit
Variablen als Parameter gestalten. In Zukunft könnte man das auch mit
Makros in \texttt{startup.S} lösen. Das Ziel einer Reduzierung des
\textbf{Assembler-Codes} und dessen Zusammenfassung in wenige Dateien
ist dennoch gelungen: \textbf{221 statt 620} Zeilen.

Was die Zukunftssicherheit durch die Migration auf eine moderne
Architektur angeht, so stellt hhuOS-for-Pi4 einen ersten Schritt dar.
Der Raspberry Pi4 soll als vereinfachtes \emph{Compute Module} bis
mindestens Januar 2028\footnote{\url{https://www.raspberrypi.org/products/compute-module-4/?variant=raspberry-pi-cm4001000}}
hergestellt werden. Verglichen mit den Pi-Vorgänger"-modellen ist keine
erneute große Veränderung der Hardware bei weiteren Versionen zu
erwarten. Ähnlich wie \protect\hyperlink{xv6}{xv6} könnte der Schritt
gegangen werden, und statt einer ARM-Architektur ein RISC-V Prozessor
(und Emulator) verwendet werden. Mit hhuOS-for-Pi4 sollte dieser Schritt
leichter Fallen, als in der x86er Version. Interessant ist, dass
Portierungen von xv6 auf den Pi bislang ebenfalls keine USB-Tastatur
implementiert haben und manche minimalistischen Projekte wie das
\protect\hyperlink{mmbasic-und-kleinst-rechner}{MicroMite-Board} und die
ersten Versionen von \protect\hyperlink{topsy}{Topsy} statt einer
Grafik-Ausgabe und einer Tastatur-Eingabe nur eine serielle
Schnittstelle anbieten. In hhuOS-for-Pi4 sind ca. 800 Codezeilen nur zur
Darstellung von Buchstaben auf dem Bildschirm nötig, was die Überlegung
einer reinen seriellen Ausgabe zur Code-Reduzierung legitimiert.

Ein neues Grundsystem von hhuOS-for-Pi4 ist mit 3792
\protect\hyperlink{codehhuos4pi4}{C++-Codezeilen} sehr klein, wenn man
es mit Minix (20247), OOStuBS (12691) und dem derzeitigem hhuOS (ca.
27753) vergleicht. Das Grundsystem von 2017 mit ca. 4440 C++-Codezeilen
(ohne Code der Shell-Anwendung) ließ sich entsprechend der Zielsetzung
verkleinern. Allerdings fehlt bisher in hhuOS-for-Pi4 eine akzeptable
PS/2- oder USB-Tastatur-Anbindung sowie ein kleiner Teil an Code, der in
\cite{ddosbasic} bereits ein Paging ermöglichte.

Für die Dokumentation von hhuOS-for-Pi4 wurde mit dieser Ausarbeitung
und der In-Code-Dokumentation mit Doxygen eine Basis für eine zukünftige
Weiterentwicklungen auf dem Raspberry Pi4 geschaffen. Sie ist auch zur
Migration anderer Lehr-Betriebssysteme auf den Pi4 verwendbar. Final
kann bewertend gesagt werden, dass im Gegensatz zu den meisten
Baremetal-Beispielen für den Pi und den meisten Lehr-Betriebs"-systemen
hhuOS-for-Pi4 primär in C++ geschrieben ist und bildet damit auch eine
Besonderheit unter den Baremetal-Beispielen für den Pi4. Scheduler und
Threads sowie die Nutzung des neuen Interrupt-Controller GIC-400 machen
hhuOS-for-Pi4 zu einer Alternative für Circle und freeRTOS.

\begin{longtable}[]{@{}lrr@{}}
\caption{Die Tabelle zeigt die Codezeilen von hhuOS-for-Pi4 in einer
komplett-Version und als neues
Grundsystem\protect\hypertarget{codehhuos4pi4}{}{.}}\tabularnewline
\toprule
Bedeutung & Codezeilen & Codezeilen \\
\midrule
\endfirsthead
\toprule
Bedeutung & Codezeilen & Codezeilen \\
\midrule
\endhead
Bluescreen & 341 & 140 \\
PCIe \& USB & 780 & 0 \\
PS/2 Tastatur & 431 & 431 \\
BMP viewer & 35 & 0 \\
Seriell (6x UART) & 286 & 0 \\
DMA/Audio & 354 & 354 \\
Logo \& Schriften & (16797) & (2055) \\
Shell & 343 & 210 \\
Eingabe (Interact) & 142 & 51 \\
Anwendungen & 952 & 86 \\
Memory Management (Gready) & 106 & 88 \\
Heap Linked-List & 306 & 0 \\
Heap Bitmask & 287 & 0 \\
Grafik-/Textausgabe & 906 & 800 \\
Scheduler & 324 & 324 \\
startup.S & 213 & 197 \\
Kernel (Rest) & 1243 & 1111 \\
\textbf{Summe:} & \textbf{7049} & \textbf{3792} \\
\bottomrule
\end{longtable}

\textbf{Ausblick}

Für die Zukunft ist geplant, einen Zugriff auf das FAT32-Dateisystem der
SD-Karte zu implementieren. Sobald der Dateizugriff und der Code zur
PS/2-Tastatur tadellos funktioniert, wäre eine Basis für ein autonomes
System geschaffen, das ohne einen zweiten Rechner bedient werden kann.
Der Zugriff auf den SPI- und I2C-Bus des Pi4 ist für Sensoren und andere
Ein- und Ausgabe-Geräte wie z.B. ein TFT-Touch-Display ebenfalls
denkbar.

Die nächsten Funktionen, die man in hhuOS-for-Pi4 einbauen könnte,
stehen dem Konzept entgegen, ein einfaches und übersichtliches
Betriebssystem für die Lehre zu haben. Allerdings sind Semaphoren,
Kernel/User-Space-Trennung und Multicore keine unwichtigen Themen in der
Betriebssystem-Entwicklung. Hier muss individuell eine Grenze gezogen
werden, wie komplex und umfangreich hhuOS-for-Pi4 werden soll. Eine Idee
wäre zum Beispiel, das Git-Repository in ein immer umfangreicher
werdendes Tutorial abzuändern.

Da ein paar Calls bereits beispielhaft implementiert sind, steht einem
Konzept zur Umschaltung von Kernel- und User-Mode nicht viel im weg. Die
Calls können auch zur Einbindung von Newlib (siehe
\texttt{newlib0}-Ordner von \cite{dwelch}) dienen, um typische
LibC-Funktionen wie \texttt{malloc} und \texttt{printf} zu haben.
Ebenfalls offen ist das Aktivieren der drei weiteren A72 Kerne. Dafür
muss dann ein passendes Konzept für Scheduling, eigene Stacks und
Interrupts durch den ARM-Massagebus gefunden werden. Eine getrennte
Heap-Verwaltung je Kern ist da die erste Ebene, mit der man
improvisieren kann. Die Implementierung von Semaphoren über eine
Warteliste im Scheduler ist auch ratsam. Das Einschalten der MMU und der
Nutzung eines 1:1 Mappings von virtuellen auf physikalische Adressen
wäre ein weiterer Schritt. Sobald letzteres gelingt, kann auch der
ARM-Spinlock umgesetzt werden. Mit einer virtuellen Adressierung kann
man es nochmal mit der Konfiguration der PCIe-Schnittstelle versuchen,
die hier eine Trennung von Adressbereichen je gefundenem Gerät vorsieht.
Die Verwendung der Gigabit-Schnittstelle und des Bluetooth-Chips wäre
ein weiteres größeres Projekt, da das Einspielen einer Firmware in die
Chips beim oder nach dem Booten eines Baremetal-Kernels kaum
dokumentiert ist\footnote{Seit Mitte Oktober 2021 gibt es zu dieser
  \cite{pi4tut} Referenz Neuigkeiten. Sowohl eine Interruptbehandlung
  als auch ein Ethernet-Zugriff wird dort für den Pi4 entwickelt.}.

Im Laufe der Implementierung hat sich gezeigt, dass sich der Pi4 vor
allem wegen des Closed-Source VideoCores VI nicht zu 100\% emulieren
lässt. Eine Portierung von hhuOS-for-Pi4 auf ein offenes und
emulierbares System wie RISC-V wäre möglich. Auch die Portierung auf das
\emph{BeagleBone Black} mit einem ARM Cortex-A8 (32-Bit) wäre eine Idee.
Erste Schritte, hhuOS-for-Pi4 auf einem Pi1 oder Pi-Zero mit ARM-32-Bit
Code laufen zu lassen, sind bereits unternommen worden. Vor allem die
Peripherie-Adressen, die Funktionsweise des Interrupt-Controllers und
die \texttt{startup.S} müssen dafür angepasst werden.
